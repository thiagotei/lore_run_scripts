#!/bin/bash

BASEDIR=`pwd`
#ICEBIN=/home/tteixei2/thiago/ice-personal/ice-locus-opentuner.py
ICEBIN=ice-locus-opentuner-loredb.py
NTESTS=500
#NTESTS=5


trap "exit" INT
while read p;
do
	vars=($p)
	dirstr=${vars[0]}
	loopstr=${vars[1]}
	# Copy timing function
	cp -v newtiming.py $dirstr

	# Copy locus file
	cp -v scop.locus $dirstr

	cp -v Makefile $dirstr

	#sfinfoitmp=${loopstr%%.vec}
	sfinfoitmp=${loopstr%%_main.c}

	cd $dirstr
	if [ $? -eq 0 ]
	then
		#loopfile=${loopstr%%.vec}_loop.c.preproc.c
		loopfile=${loopstr%%_main.c}_loop.c.preproc.c
		echo Starting... $dirstr $loopfile $loopstr

		# Change locus file
		#sed -i "s#VECFILENAME#${loopstr}#g" scop.locus
		sed -i "s#VECFILENAME#./${sfinfoitmp}.CCMODE#g" scop.locus
		sed -i "s#INFOFILENAME#${sfinfoitmp}.CCMODE#g" scop.locus
		
		# Copy to ice so it can be used by original execution.
		# Makefile only accepts _ice.c files :(
		cp -v $loopfile ${loopfile%%preproc.c}preproc_ice.c

		rm -rf opentuner.db *.log rose_tmp-* tmp-*
		# Run
		#arrBenchInfo=(${dirstr//\// } ${loopstr//_/ })
		#benchInfotmpA=${arrBenchInfo[@]:3:3}" "${arrBenchInfo[@]:7:3}
		#benchInfotmpB=${benchInfotmpA// /:}  #replace space by colon to be passed to ice
		#benchInfo=${benchInfotmpB//line/} #remove the line word from the last field

		arrBenchInfo=(${dirstr//\// })
		bch_file=${sfinfoitmp%%.c*}
		bch_nucl_A=${sfinfoitmp##*.c}
		bch_nucl_B=${bch_nucl_A%%_line*}
		bch_nucl=${bch_nucl_B#_}
		bch_line=${sfinfoitmp##*_line}
		benchInfotmpA=${arrBenchInfo[@]:3:3}" "${bch_file}" "${bch_nucl}" "${bch_line}
		benchInfo=${benchInfotmpA// /:}  #replace space by colon to be passed to ice

		#sfinfo=${sfinfoitmp##./}
		sfinfo=${sfinfoitmp}
		cmd="$ICEBIN -f $loopfile -t scop.locus -o suffix --search --ntests $NTESTS \
			--tfunc newtiming.py:getTiming --suffixinfo ${sfinfo} \
			--loredb_save --loredb_user tteixei2 --loredb_ip 192.17.58.228  \
			--loredb_pass polaris --loredb_bench ${benchInfo} --equery --debug"

	#	cmd="$ICEBIN -f $loopfile -t scop.locus -o suffix --search --ntests $NTESTS \
	#		--tfunc mytiming.py:getTimingMatMul --suffixinfo ${sfinfo} \
#			" #--debug"

		echo $cmd
		$cmd
		#cp ice.log ${sfinfo}_ice.log
		echo $dirstr $loopfile Done!
		cd $BASEDIR
	fi
done  < $1
