#!/bin/bash

BASEDIR=`pwd`

trap "exit" INT
while read p;
do
	vars=($p)
	dirstr=${vars[0]}
	loopstr=${vars[1]}
	# Copy timing function

	cd $dirstr
	echo Starting... $dirstr $loopfile $loopstr
	loopfile=${loopstr%%.vec}_loop.c.preproc.c
	cp -v $loopfile ${loopfile%%.preproc.c}.preproc_ice.c
	cd $BASEDIR
done  < $1
