#!/bin/bash


filebase=imageio.c_image_rgb24_from_rgb96f_line167
cd /home/tteixei2/benchmarks/ALPBench/Ray_Trace/tachyon/src/extractedLoops
make clean ${filebase}.vec
var=`./${filebase}.vec ../input 10 | awk 'f{print $3 "-" $0 ;f=0} /min/{f=1}'`
echo $var

