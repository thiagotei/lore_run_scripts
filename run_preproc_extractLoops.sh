#!/bin/bash

BASEDIR=`pwd`
PREPROCBIN=/home/tteixei2/thiago/rose-dev/buildBAddDep/loripreproc/loripreproc
NTESTS=5

while read p;
do
	vars=($p)
	dirstr=${vars[0]}
	loopstr=${vars[1]}
	cd $dirstr
	loopfile=${loopstr%%.vec}_loop.c
	echo Starting... $dirstr $loopfile $loopstr
	
	# Run
	cmd="${PREPROCBIN} ${loopfile}"
	echo $cmd
	$cmd
	[ $? -eq 0 ] && echo $dirstr $loopfile Done!
	cd $BASEDIR
done  < $1
