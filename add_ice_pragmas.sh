#!/bin/bash

BASEDIR=`pwd`
while read p;
do
	vars=($p)
	dirstr=${vars[0]}
	loopstr=${vars[1]}
	cd $dirstr
	#loopfile=${loopstr%%.vec}_loop.c.preproc.c
	loopfile=${loopstr%%_main.c}_loop.c.preproc.c
	sed -i 's/pragma scop/pragma @ICE loop=scop/g' ${loopfile}
	sed -i '/pragma endscop/d' ${loopfile}
	echo $dirstr $loopfile Done!
	cd $BASEDIR
done  < $1
