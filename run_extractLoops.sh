#!/bin/bash
BASE_DIR=`pwd` #"."
BENCHDIRS=`ls -1 $BASE_DIR`
#BENCHDIRS=ALPBench
#BENCHDIRS=NPB
#BENCHDIRS="opencv_kernels polybench scimark2 spec2000 spec2006 TSVC"
echo $BENCHDIRS being assesed!

trap "exit" INT
for bdir in $BENCHDIRS;
do
	if [ -d $bdir ]
	then
		echo "## Starting" $bdir "..."
		cd $bdir
		for loopsdir in `find . -name extractedLoops`;
		do
			cd $loopsdir
			echo "===" $loopsdir
			#for file in `ls -1 ${loopsdir}/*_main.c`;
			for file in `ls -1 *_main.c`;
			do
				filebase=${file%%_main.c}
				filebasepreprocp=${filebase}_loop.c.preproc.c
				filebasepreprocpICE=${filebase}_loop.c.preproc_ice.c
				cp -v $filebasepreprocp $filebasepreprocpICE
				path=`pwd`
				resstr="${path} ${file}" 
				#echo -n "${path} ${file} YESpluto"
				echo "Compiling version"
				make clean ${filebase}.vec &>> ${BASE_DIR}/make_results.txt  #/dev/null
				if [ $? -eq 0 ]
				then
					#echo -n " YEScompiles! "
					resstr=${resstr}" YEScompiles! "
					bincmd="./${filebase}.vec ../input 10"
					#ls -la *.vec
					#./${filebase}.vec ../input 10
					echo "Running version"
					bufres=$(${bincmd}) # | awk '/min/{getline; print $3}`
					if [ $? -eq 0 ]
					then
						#echo $bufres
						#meanvalue=`echo ${bufres} | awk '/min/{getline; print $3}' `
						meanvalue=`echo ${bufres} | awk '{print $(NF-2)}' `
						#echo -n " YESruns! meanexectime: ${meanvalue}"
						resstr=${resstr}" YESruns! meanexectime: ${meanvalue}"
					else
						#echo -n " NOruns!"
						resstr=${resstr}" NOruns!"
					fi
				else
					#echo -n " NOcompiles!"
					resstr=${resstr}" NOcompiles!"
				fi
				#echo " "
				echo $resstr
			done
			cd $BASE_DIR/$bdir
		done
	fi
	cd $BASE_DIR
done

