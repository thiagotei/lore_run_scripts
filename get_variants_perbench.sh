#!/bin/bash

die () {
	echo >&2 "$@"
	exit 1
}

[ -f "${1}" ] || die "File ${1} does not exist"

BENCHS="ALPBench ASC-llnl cortexsuite FreeBench Kernels libraries livermore machinelearning mediabench netlib NPB polybench scimark2 spec2000 spec2006 TSVC GitApps"
for bench in $BENCHS;
do

echo -n Running $bench " "
#awk -v bench=${bench} 'BEGIN{variants=0}/bench/{print $10,variants; variants+=$10;}END{print variants}' ${1}
res=`awk -v VAR_NAME=${bench} 'BEGIN{loops=0;variants=0} $0 ~ VAR_NAME {variants+=$10; loops+=1;}END{print "loops: " loops " variants: " variants }' ${1}`
echo $res


done
