#!/bin/bash

basenameout="processing_output_"`date +%Y%m%d_%H`".txt"
grep -h "Starting\.\.\.\|Result metric for original\|Final Best\|Done!" run1_p* > $basenameout
# put everything in one line
~/lori_scripts/parse_icelog_output.awk $basenameout > ${basenameout}.step2
# remove cases that do not have a Done!
sed -e "s/^Starting.*Starting/Starting/" ${basenameout}.step2 > ${basenameout}.step3
# Select the fields
awk '(NF >= 27) && ($27 != "inf"){print $2 " " $3 " " $13 ": " $14 " " $26  " " $27 " " $28 " " $29 " " $30 " " $31  " "}' ${basenameout}.step3 > ${basenameout}.final
