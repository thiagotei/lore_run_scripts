#!/bin/bash
BASE_DIR=`pwd` #"."
BENCHDIRS=`ls -1 $BASE_DIR`

for bdir in $BENCHDIRS;
do
	echo "## Starting" $bdir "..."
	cd $bdir
	for i in `find $BASE_DIR -name extractedLoops`;
	do
		#cd $i
		for file in `ls -1 ${i}/*_main.c`;
		#for file in `ls -1 *_main.c`;
		do
			#echo $file
			basename=${file%%_main.c}
			dir=`dirname $file`
			#echo $basename
			make ${basename}".vec" &> /dev/null;
			if [ $? -eq 0 ]
			then
				echo ${basename}" compiles!"
				#${basename}".vec ${dir}/../input 10"
				backdir=`pwd` 
				cd ${dir}
				${basename}".vec ../input 10"
				#ls ${basename}".vec"
				#ls "${dir}/../input"
				if [ $? -eq 0 ]
				then
					echo ${basename}" runs!"
				else
					echo "No run!"
				fi
				echo "++++++++"
				cd ${backdir}
				pwd
			else
				echo "No compile!"
			fi
		done
		#cd ..
	done
	cd ..
done

