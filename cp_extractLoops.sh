#!/bin/bash

#BASE_DIR=/home/zhi/benchmarks/loop_collections
BASE_DIR=/shared/loop_collections_10242017
BENCHDIRS=`ls -1 $BASE_DIR`
#BENCHDIRS=ALPBench

for bdir in $BENCHDIRS;
do
	[ ! -d ${BASE_DIR}/$bdir ] && continue
	echo "Starting" $bdir "..."
	mkdir $bdir
	cd $bdir
	for i in `find ${BASE_DIR}/${bdir} -name extractedLoops`;
	do
		dst=`echo $i | cut -d '/' -f 5-`
		mkdir -p $dst
		echo orig $i dst $dst
		for ploops in `find ${i} -name *.preproc.c`
		do
			echo "@@@ ### %%%" $ploops
			#ploops=`cut -d: -f 1- ${ploops}`
			cp  $ploops $dst
			cp  ${ploops%%_loop.c.preproc.c}_main.c $dst
		done
		MKPATH=${i}/Makefile
		cp  ${MKPATH}  $dst
		sed -i 's/_loop.c/_loop.c.preproc_ice.c/g' ${dst}/Makefile
		cp  ${i}/isolatedLoopUtility.a $dst
	done
	for i in `find ${BASE_DIR}/${bdir} -name input`;
	do
		dst=`echo $i | cut -d '/' -f 5-`
		ln -s $i `dirname $dst`
	done
	echo "Ended" $bdir
	cd ..
done

