#!/usr/bin/env python

import argparse, math, sys

parser = argparse.ArgumentParser(description='Compare outputs for extracted loops on LoreDb.')
parser.add_argument("-a", type=str, required=True, help="Original file name path.")
parser.add_argument("-b", type=str, required=True, help="To compare file name path.")

results = parser.parse_args()
code = 0
with open(results.a, 'r') as inporig, open(results.b,'r') as inpcurr:
    for line1, line2 in zip(inporig, inpcurr):
        if "f.p. array" in line1:
            # Compare the this field up delta 10-4 difference.

            val1 = float(line1.split()[-1])
            val2 = float(line2.split()[-1])
            #print val1, val2, math.fabs(val1-val2) >= 10e-4
            #if math.fabs(val1-val2) >= 10e-4:
            if math.fabs(val1-val2) > math.fabs(10e-6*val1):
                print "Difference bigger: abs(", val1,"-", val2, ")", math.fabs(val1-val2)
                code = 2
                break

        elif "min\tmax\tmean\tmedian\tstdv" in line1:
            # Comparison should stop when found this line.
            break
        else:
            if line1 != line2:
                print "Different: ", line1, line2
                code = 1
                break
sys.exit(code)
